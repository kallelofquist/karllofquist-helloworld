// Karl Löfquist

import java.util.ArrayList;
import java.util.List;

public class Program {

	public static List<String> familyNames = new ArrayList<>();

	public static void main(String[] args) {
		
		familyNames.add("Jeanette");
		familyNames.add("Andreas");
		familyNames.add("Kalle");
		familyNames.add("Viggo");
		familyNames.add("Ture");
		familyNames.add("Ove");

		printNames();

	}

	public static void printNames() {

		for (String name : familyNames) {

			System.out.println(name);
			
		}

	}

}